# Spring Boot DTO

Returning Data Transfer Objects from a Rest Controller in Spring Boot.

A few things to notice
```
@JsonFormat(pattern = "dd/MM/yyyy")
```

Formats the date when the DTO is output to JSON. If this is not used the JSON will display a number that represents the time instead of a easy to read string.
```
@DateTimeFormat(pattern = "dd/MM/yyyy")
```
This works the other way around as it allows the date to be input in dd/MM/yyyy format, which if your trying to pass a date directly into JSON it will be hard to know the number version of the date you want.

Also remember to include your default constructor, getters and setters otherwise serializing and deserializing of the DTO will not work.

`GET /getPersonDTOList`

Endpoint :
```
http://localhost:8080/getPersonDTOList?personDTO={"firstName":"First name","secondName":"Second name","profession":"Professional time waster","salary":0,"dateOfBirth":"01/12/2020"}&personDTO2={"firstName":"Random first name","secondName":"Random second name","profession":"Professional sleeper","salary":123,"dateOfBirth":"11/12/2100"}``
```

![Get Person Data](img/person.png "Get Person Data")

As you can see from the output each PersonDTO is contained within the square brackets that represent the list in JSON. So did anything interesting happen in the code or the return data? Nope, still nice a simple.

The last example is slightly different from first looks but it is pretty much the same as returning the List<PersonDTO>.

Now that you have seen the PeopleDTO code your understand why is so similar to the previous example as it is just an object that contains a List<PersonDTO>. You might prefer to return this DTO rather than a List<PersonDTO> but I wont make that decision for you.

`GET /getPeopleDT`

Endpoint :
```
http://localhost:8080/getPeopleDTO?personDTO={"firstName":"First name","secondName":"Second name","profession":"Professional time waster","salary":0,"dateOfBirth":"01/12/2020"}&personDTO2={"firstName":"Random first name","secondName":"Random second name","profession":"Professional sleeper","salary":123,"dateOfBirth":"11/12/2100"}
```

![Get People Data](img/people.png "Get People Data")

Yet again the code required to set this up is pretty simple. Other than creating the PeopleDTO to store the List<PersonDTO> nothing else in the code has changed. The JSON output is slightly different from the previous example as the list is now tied to the people property.

So what did you learn from reading this post? Not much actually as returning a data transfer object from a Rest Controller is actually pretty straight forward. Simply set up your DTO correctly with a default constructor, getters and setters and maybe add some annotations if your feeling more sophisticated, after that there’s not really anything left to do.

